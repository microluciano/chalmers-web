This is the code behind http://www.cse.chalmers.se/~bello/

Feel free to use it under the terms of [DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE](http://www.wtfpl.net/txt/copying/), except many of the icons, which are based on [iconSweets2](http://www.iconsweets2.com/).
[iconSweets2](http://www.iconsweets2.com/) have been made by [Yummygum](http://yummygum.com/) under the terms of [CC Attribution 3.0 Unported](http://creativecommons.org/licenses/by/3.0/">CC Attribution 3.0 Unported)
