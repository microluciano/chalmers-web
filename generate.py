#!/usr/bin/python

from debian.deb822 import Deb822 as rfc822
import fileinput
import os
import shutil
from datetime import date

f=file('layout.txt')
result="www"
header="content/_header.html"
footer="content/_footer.html"
toreplace={
    'year': str(date.today().year),
    'date': date.today().strftime('%b %d, %Y'),
}

if os.path.exists(result): shutil.rmtree(result)
print "Moving static files"
shutil.copytree('content/static',result)

def replacePlaceHolders(line):
    for i,j in toreplace.iteritems():
        line=line.replace('[['+i+']]',j)
    return line

layout=rfc822(f)
while len(layout)!=0:
     page=layout['page']
     toreplace['pagetitle']=page
     location=layout['location']
     destination=os.path.join(result,location)
     destinationDirName=os.path.dirname(destination)
     content=os.path.join('content',location)
     toreplace['stylepath']=os.path.relpath('www/style.css',destinationDirName)
     toreplace['faviconpath']=os.path.relpath('www/favicon.ico',destinationDirName)
     toreplace['rootpath']=os.path.relpath('www/',destinationDirName)

     print "Generating %s at %s"%(page,destination)
     if not os.path.exists(destinationDirName): os.makedirs(destinationDirName)
     with open(destination, 'w') as fout:
             for line in fileinput.input([header,content,footer]):
                         line=replacePlaceHolders(line)
                         fout.write(line)
     layout=rfc822(f)
